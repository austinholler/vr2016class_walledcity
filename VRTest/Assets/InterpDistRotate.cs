﻿using UnityEngine;
using System.Collections;

/* TRIGGER script: This class pertains to trigger boxes, where the distances between two points are used to interpolate*/
/* Comments with @@@ are instructions on adapting the script to the real map */

public class InterpDistRotate : MonoBehaviour
{
    private RotateOnTrigger world_ptr;  // @@@"Pointer" that links to the variables of the RotateOnTrigger class.
                                       // @@@Change the object type to the name of the other script's class and change the var
                                       // @@@to whatever is useful

    GameObject start; // The point used for one side of the bridge, keep the name start_point
    GameObject end; // The point used for the other side of the bridge, keep the name end_point
    GameObject player; // Player location, keep the name player_point

    public float tween_result; // Where the world(angle) was sitting last
    private float ratio = 0; // Var THIS script uses to tell how far player is through the box
    private float endvalue; // Desired wall of the map to snap to (i.e. 0 = wall closer to start_point, 1 = wall closer to end_point)
    private bool exited_before_tween_finish = false; // For an edge case, see below

    void OnTriggerEnter(Collider other)
    {
		world_ptr.setPlayerInTriggerStatus (true);

		// When box is entered,
		if (transform.Find ("X")) {
			world_ptr.setRotationAxis ("X");
			if (transform.Find ("Neg")) {
				world_ptr.setIsNegRotate(true);
			} 
			else {
				world_ptr.setIsNegRotate (false);
			}
		} 
		else if (transform.Find ("Y")) {
			world_ptr.setRotationAxis ("Y");
			if (transform.Find ("Neg")) {
				world_ptr.setIsNegRotate(true);
			}
			else {
				world_ptr.setIsNegRotate (false);
			}
		} 
		else {
			world_ptr.setRotationAxis ("Z");
			if (transform.Find ("Neg")) {
				world_ptr.setIsNegRotate(true);
			}
			else {
				world_ptr.setIsNegRotate (false);
			}
		}


		if (transform.Find ("90")) { world_ptr.setInitialAxis (90f); } 
		else /*if (transform.Find ("0"))*/ { world_ptr.setInitialAxis (0f); }


        //Debug.Log("Object entered");
        world_ptr.stopTween(); // Stop any tweening
        setDistRatioInTrigger(); // Update "ratio"
        world_ptr.smoothTween(tween_result, ratio, 1); // Move world from where it was to where the player is
    }



    void OnTriggerStay(Collider other)
    {
        setDistRatioInTrigger(); // Update "ratio" continuously
		world_ptr.setEndValue(ratio); 
    }



    void OnTriggerExit(Collider other)
    {
		world_ptr.setPlayerInTriggerStatus (false);

		if (world_ptr.isTweening()) { exited_before_tween_finish = true; } // Edge case. This raises a flag that tweening
                                                                          // needs to be called in Update()

        //Debug.Log("Exited with ratio" + ratio);
        // Tell the world to smootly tween
        if (ratio > 0.5) //Snapping. If they jump off the bridge, snap to a wall
            endvalue = 1;
        else
            endvalue = 0;
        world_ptr.smoothTween(ratio, endvalue, 2);
    }

    void setDistRatioInTrigger() //Update the distance ratio 
    {
        Vector3 start_pos = start.transform.position;
        Vector3 end_pos = end.transform.position;
        Vector3 player_pos = player.transform.position;

        float distance_from_start = Vector3.Distance(start_pos, player_pos);
        float distance_from_end = Vector3.Distance(end_pos, player_pos);
        ratio = distance_from_start / (distance_from_start + distance_from_end);
    }

    void Start()
    {
        //@@@ ALTER:
        world_ptr = GameObject.Find("Map_center").GetComponent<RotateOnTrigger>(); //@@@ 'pointer' used to access vars from the Map's class
		start = transform.Find("startpoint").gameObject; //@@@ The point used for one side of the bridge, keep the name start_point
		end = transform.Find("endpoint").gameObject; //@@@ The point used for the other side of the bridge, keep the name end_point
        player = GameObject.Find("FPSController"); //@@@ Player location, keep the name player_point
    }

    void Update()
    {
        tween_result = world_ptr.getTweenResult(); // Continuously get the eased ratio

        // Ghetto callback addressing problem where trigger is exited before tween finishes
        if (exited_before_tween_finish && !world_ptr.isTweening()) { world_ptr.smoothTween(ratio, endvalue, 2); exited_before_tween_finish = false; }

    }

}

