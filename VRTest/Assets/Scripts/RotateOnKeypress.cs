﻿using UnityEngine;
using System.Collections;

public class RotateOnKeypress : MonoBehaviour {

    public bool x_pos_rotation_active = false;
    public bool x_neg_rotation_active = false;
    public float angle_sum = 0;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        // POS_X rotation on XBox B press
		rotatePosX(Input.GetKeyDown(KeyCode.JoystickButton1));
        rotatePosX(Input.GetKeyDown(KeyCode.Q));
        /////////////

        // Test to see if player is in the InRotateSpace box



        // NEG_X rotation on XBox X press
        rotateNegX(Input.GetKeyDown(KeyCode.JoystickButton2));
        rotateNegX(Input.GetKeyDown(KeyCode.E));
    }

    // rotatePosX - When argument "trigger" becomes true, calls RotateMap() until angle limit is reached
    public void rotatePosX(bool trigger)
    {
        if (trigger)
        {
            x_pos_rotation_active = true;
            x_neg_rotation_active = false;
            angle_sum = (angle_sum >= 90) ? (angle_sum - 90) : angle_sum;
        }

        // Rotate till 90 deg is reached
        else if (x_pos_rotation_active == true && angle_sum < 90)
        {
            rotateMap(Vector3.left, false);
        }
        else if (x_pos_rotation_active && angle_sum >= 90)
        {
            // Then Stop
            x_pos_rotation_active = false;
        }
    }

    // interpolatePosX - When argument "trigger" becomes true, calls RotateMap() until angle limit is reached
    public void interpolatePosX(bool trigger, GameObject startcap, GameObject endcap)
    {
        if (trigger)
        {
            x_pos_rotation_active = true;
            x_neg_rotation_active = false;
            angle_sum = (angle_sum >= 90) ? (angle_sum - 90) : angle_sum;
        }

        // Rotate till 90 deg is reached
        else if (x_pos_rotation_active == true && angle_sum < 90)
        {
            rotateMap(Vector3.left, false);
        }
        else if (x_pos_rotation_active && angle_sum >= 90)
        {
            // Then Stop
            x_pos_rotation_active = false;
        }
    }

    // rotateNegX - When argument "trigger" becomes true, calls RotateMap() until angle limit is reached
    public void rotateNegX(bool trigger)
    {
        if (trigger)
        {
            x_neg_rotation_active = true;
            x_pos_rotation_active = false;
            // Start from 90 degrees or whatever remains
            angle_sum = (angle_sum <= 0) ? (90 + angle_sum) : angle_sum;
        }
        // Rotate till 0 deg is reached
        else if (x_neg_rotation_active && angle_sum > 0)
        {
            rotateMap(Vector3.left, true);
        }
        else if (x_neg_rotation_active && angle_sum <= 0)
        { // Then Stop
            x_neg_rotation_active = false;
            angle_sum %= 90;
        }
    }

    // rotateMap - Rotates map by one increment per call around the 'direction' axis, in the 'isNegative' direction
    public void rotateMap(Vector3 direction, bool isNegative)
    {
        int neg_mult = (isNegative) ? -1 : 1;
        float rotation_rate = 20 * Time.deltaTime * neg_mult;
        // Perform rotation ABOUT THE PLAYER
        transform.RotateAround(GameObject.Find("FPSController").transform.position, direction, rotation_rate);
        angle_sum += rotation_rate;
    }

}
