﻿using UnityEngine;
using System.Collections;

/* MAP script: Functions inside here modify the properties of the map*/
/* Comments with @@@ are instructions on adapting the script to the real map */

public class RotateOnTrigger : MonoBehaviour
{

    /* smoothTween - uses a sine easing function to smoothly tween between two values over the given time */
    private bool tweening = false; // Used to tell if tween is in progress
    private float result = 0;
    private float starttime, startvalue, endvalue, time_sec, variable;
	private string rotationAxis;
	private bool isNegRotate;
	private float initialAxisRotation;

	// Allows script to be informed of the trigger's state
	public bool playerInTrigger;

    // Getters and setters for the other script
    public bool isTweening() { return tweening; }
    public float getTweenResult() { return result; }
    public void setEndValue(float in_endvalue) { endvalue = in_endvalue; }
	public void setRotationAxis(string _axis) { rotationAxis = _axis; }
	public void setIsNegRotate(bool _direction) { isNegRotate = _direction; }
	public void setInitialAxis(float _intialAxisValue){ initialAxisRotation = _intialAxisValue; }
	public void setPlayerInTriggerStatus(bool _in) { playerInTrigger = _in; }

    // RUN THIS FUNCTION to do easing. Can be run in two ways: 1. All 3 arguments when initializing  2. No arguments to move it along in the update function
    public void smoothTween(float _startvalue = 0, float _endvalue = 0, float _time_sec = 0)
    {
        // If the tween is just starting
        if (!tweening && _time_sec != 0)
        {
            starttime = Time.realtimeSinceStartup; startvalue = _startvalue; endvalue = _endvalue; time_sec = _time_sec;
            tweening = true;
        }
		// If player in trigger, directly alter the rotation
		if (!tweening && playerInTrigger) {
			setWorldRotation (endvalue);
		}
        if (tweening)
        {
            // Detect if the tweening end has been reached
            if (Time.realtimeSinceStartup >= (time_sec + starttime))
            {
                stopTween();
                setWorldRotation(endvalue);
            }
            else
            {
                setWorldRotation(calculateTween());
            }
        }
    }
    public void stopTween()
    {
        starttime = 0;
        tweening = false;
    } //Used to cut the tween short if necessary
    private float calculateTween()
    {
        float progress = (Time.realtimeSinceStartup - starttime) / time_sec;
        double ease_factor = (-Mathf.Cos(Mathf.PI * progress) / 2) + 0.5; // Wolfram alpha it and see the curve from x=0 to x=1!
        result = (float)(startvalue + ((endvalue - startvalue) * ease_factor));
        return result;
    }
    /* -----------------------------------------------*/

    //@@@ THE SETTING FUNCTION FOR THE MAP ANGLE
    /* setWorldRotation - public function that takes a ratio from 0 to 1 and applies redness */
    public void setWorldRotation(float ratio)
    {

		float currentAngle;
		float angleRatio;
		float angleChange;
		Vector3 myAxis;

		/*print ("My ratio: " + ratio);
		print ("My intialDegrees: " + initialAxisRotation);
		print ("My isNegRotate is: " + isNegRotate);
		print ("My rotationAxis is: " + rotationAxis);*/


		if (rotationAxis == "X") {

			currentAngle = transform.localRotation.eulerAngles.x;

			//print ("My currentAngle X: " + currentAngle);

			myAxis = this.transform.right;
		} else if (rotationAxis == "Y") {

			currentAngle = transform.localRotation.eulerAngles.y;

			//print ("My currentAngle Y: " + currentAngle);

			myAxis = this.transform.up;
		} 
		else {
			
			currentAngle = transform.localRotation.eulerAngles.z;

			//print ("My currentAngle Z: " + currentAngle);

			myAxis = this.transform.forward;
		}

		if (isNegRotate) {
			angleRatio = -90 * ratio;
		} 
		else { angleRatio = 90 * ratio; }

		//print ("My angleRatio is: " + angleRatio);

		angleChange = (initialAxisRotation + angleRatio) - currentAngle;

		//print ("My angleChange is: " + angleChange);

		transform.RotateAround(GameObject.Find ("FPSController").transform.position, myAxis, angleChange);

    }
    /* ----------------------------------------------*/

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // Enable tweening "callback"
        smoothTween();

    }

}
