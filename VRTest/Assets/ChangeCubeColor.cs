﻿using UnityEngine;
using System.Collections;

/* MAP script: Functions inside here modify the properties of the map*/
/* Comments with @@@ are instructions on adapting the script to the real map */

public class ChangeCubeColor : MonoBehaviour {

    /* smoothTween - uses a sine easing function to smoothly tween between two values over the given time */
    private bool tweening = false; // Used to tell if tween is in progress
    private float result = 0;
    private float starttime, startvalue, endvalue, time_sec, variable;

    // Getters and setters for the other script
    public bool isTweening() { return tweening; }
    public float getTweenResult() { return result; }
    public void setEndValue(float in_endvalue) { endvalue = in_endvalue; }

    // RUN THIS FUNCTION to do easing. Can be run in two ways: 1. All 3 arguments when initializing  2. No arguments to move it along in the update function
    public void smoothTween(float _startvalue = 0, float _endvalue = 0, float _time_sec = 0)
    {
        // If the tween is just starting
        if (!tweening && _time_sec != 0)
        {
            starttime = Time.realtimeSinceStartup; startvalue = _startvalue; endvalue = _endvalue; time_sec = _time_sec;
            tweening = true;
        }
        if (tweening)
        {
            // Detect if the tweening end has been reached
            if (Time.realtimeSinceStartup >= (time_sec + starttime))
            {
                stopTween();
                setCubeColor(endvalue);
            }
            else
            {

                setCubeColor(calculateTween());
            }
        }
    }
    public void stopTween()
    {
        starttime = 0;
        tweening = false;
    } //Used to cut the tween short if necessary
    private float calculateTween()
    {
        float progress = (Time.realtimeSinceStartup - starttime)/time_sec;
        double ease_factor = (-Mathf.Cos(Mathf.PI * progress) / 2) + 0.5; // Wolfram alpha it and see the curve from x=0 to x=1!
        result = (float)(startvalue + ((endvalue - startvalue) * ease_factor));
        return result;
    }
    /* -----------------------------------------------*/

    //@@@ THE SETTING FUNCTION FOR THE MAP ANGLE
    /* setCubeColor - public function that takes a ratio from 0 to 1 and applies redness */
    public void setCubeColor(float ratio)
    {
        //Color col = new Color(ratio, 0, 1 - ratio); //@@@ Here the values are from 0-1, so with map angles you would use (ratio*90)
        //gameObject.GetComponent<Renderer>().material.SetColor("_Color", col);
        float angle = 90 * ratio;

        transform.rotation = Quaternion.Euler(angle, 0, 0);
        //transform.position = new Vector3(17+(11 * ratio), -18, (float)2.75); //@@@ Move the cube from side to side, so with map angles you would use (ratio*90)
    }
    /* ----------------------------------------------*/

    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        // Enable tweening "callback"
        smoothTween();

    }

}
