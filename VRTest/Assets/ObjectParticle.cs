﻿using UnityEngine;
using System.Collections;

public class ObjectParticle : MonoBehaviour {

	// Use this for initialization
	void Start () {
		ParticleSystem ps = GetComponent<ParticleSystem> ();
		var coll = ps.collision;
		coll.enabled = true;
		coll.bounce = 0.5f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
