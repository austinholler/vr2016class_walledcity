﻿using UnityEngine;
using System.Collections;

/* TRIGGER script: This class pertains to trigger boxes, where the distances between two points are used to interpolate*/
/* Comments with @@@ are instructions on adapting the script to the real map */

public class InterpDist : MonoBehaviour {
    private ChangeCubeColor cube_ptr;  // @@@"Pointer" that links to the variables of the ChangeCubeColor class.
                                       // @@@Change the object type to the name of the other script's class and change the var
                                       // @@@to whatever is useful

    GameObject start; // The point used for one side of the bridge, keep the name start_point
    GameObject end; // The point used for the other side of the bridge, keep the name end_point
    GameObject player; // Player location, keep the name player_point

    public float tween_result; // Where the cube(angle) was sitting last
    private float ratio = 0; // Var THIS script uses to tell how far player is through the box
    private float endvalue; // Desired wall of the map to snap to (i.e. 0 = wall closer to start_point, 1 = wall closer to end_point)
    private bool exited_before_tween_finish = false; // For an edge case, see below

	void OnTriggerEnter(Collider other)
    {
        // When box is entered,

        //Debug.Log("Object entered");
        cube_ptr.stopTween(); // Stop any tweening
        setDistRatioInTrigger(); // Update "ratio"
        cube_ptr.smoothTween(tween_result, ratio, 1); // Move cube from where it was to where the player is
    }
    void OnTriggerStay(Collider other)
    {
        //Debug.Log("in trigger");
        setDistRatioInTrigger(); // Update "ratio" continuously
        if (!cube_ptr.isTweening()) //If the cube is done moving into position after you enter the box
        {
            cube_ptr.setCubeColor(ratio); //@@@ MOVE THE CUBE exactly with player's position AKA no easing. 
                                            //This would be replaced with a setter function 
                                            // in the other script that sets an absolute rotation of the map.
        }
        else // This means that OnTriggerEnter easing hasn't finished, so tell easing the player is in a new spot now
        {
            cube_ptr.setEndValue(ratio);
        }
        //Debug.Log(ratio);
    }
    void OnTriggerExit(Collider other)
    {
        if (cube_ptr.isTweening()) { exited_before_tween_finish = true; } // Edge case. This raises a flag that tweening
                                                                        // needs to be called in Update()

        //Debug.Log("Exited with ratio" + ratio);
        // Tell the cube to smootly tween to one color
        if (ratio > 0.5) //Snapping. If they jump off the bridge, snap to a wall
            endvalue = 1;
        else
            endvalue = 0;
        cube_ptr.smoothTween(ratio,endvalue, 2);
    }

    void setDistRatioInTrigger() //Update the distance ratio 
    {
        Vector3 start_pos = start.transform.position;
        Vector3 end_pos = end.transform.position;
        Vector3 player_pos = player.transform.position;

        float distance_from_start = Vector3.Distance(start_pos, player_pos);
        float distance_from_end = Vector3.Distance(end_pos, player_pos);
        ratio = distance_from_start / (distance_from_start + distance_from_end);
    }

    void Start()
    {
        //@@@ ALTER:
        cube_ptr = GameObject.Find("Cube").GetComponent<ChangeCubeColor>(); //@@@ 'pointer' used to access vars from the Map's class
        start = GameObject.Find("startpoint"); //@@@ The point used for one side of the bridge, keep the name start_point
        end = GameObject.Find("endpoint"); //@@@ The point used for the other side of the bridge, keep the name end_point
        player = GameObject.Find("FPSController"); //@@@ Player location, keep the name player_point
    }

    void Update()
    {
        tween_result = cube_ptr.getTweenResult(); // Continuously get the eased ratio

        // Ghetto callback addressing problem where trigger is exited before tween finishes
        if (exited_before_tween_finish && !cube_ptr.isTweening()) { cube_ptr.smoothTween(ratio, endvalue, 2); exited_before_tween_finish = false; }

    }

}

